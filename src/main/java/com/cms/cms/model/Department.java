package com.cms.cms.model;

import lombok.Data;

@Data
public class Department {
    private int departmentid;
    private String departmentname;
}
