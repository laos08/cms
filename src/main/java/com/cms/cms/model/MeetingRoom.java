package com.cms.cms.model;

import lombok.Data;

@Data
public class MeetingRoom {
    private int roomid;
    private int roomnum;
    private String roomname;
    private int capacity;
    private String status;
    private String description;

}
