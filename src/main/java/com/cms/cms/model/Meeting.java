package com.cms.cms.model;

import lombok.Data;

@Data
public class Meeting {
    private int meetingid;
    private String meetingname;
    private int roomid;
    private int reservationistid;
    private int numberofparticipants;
    private String starttime;
    private String endtime;
    private String reservationtime;
    private String canceledtime;
    private String description;
    private String status;

}
