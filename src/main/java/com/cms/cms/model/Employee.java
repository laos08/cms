package com.cms.cms.model;

import lombok.Data;

@Data
public class Employee {
    private int employeeid;
    private String employeename;
    private String username;
    private String phone;
    private String email;
    private String status;
    private int departmentid;
    private String password;
    private String role;

}
