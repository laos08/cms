package com.cms.cms.dao;

import com.cms.cms.model.Counter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CounterDao {
    @Select("select * from counter")
    List<Counter> getAllCounter();

}
