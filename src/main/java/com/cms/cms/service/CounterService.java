package com.cms.cms.service;

import com.cms.cms.model.Counter;

import java.util.List;

public interface CounterService {
    List<Counter> getAllCounter();
}
