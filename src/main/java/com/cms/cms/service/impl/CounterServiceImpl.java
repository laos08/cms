package com.cms.cms.service.impl;

import com.cms.cms.dao.CounterDao;
import com.cms.cms.model.Counter;
import com.cms.cms.service.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CounterServiceImpl implements CounterService {
    @Autowired
    CounterDao counterDao;

    @Override
    public List<Counter> getAllCounter() {
        return counterDao.getAllCounter();
    }
}
