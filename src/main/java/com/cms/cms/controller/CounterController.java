package com.cms.cms.controller;

import com.cms.cms.model.Counter;
import com.cms.cms.service.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/system/counter/")
public class CounterController {
    @Autowired
    CounterService counterService;

    // http://localhost:8088/system/counter/getAllCounter.do
    // return [{"visitcount":99}]
    @RequestMapping("/getAllCounter.do")
    @ResponseBody
    public List<Counter> getAllCounter(){
        return counterService.getAllCounter();
    }
}
