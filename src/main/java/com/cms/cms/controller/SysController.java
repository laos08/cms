package com.cms.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


//公共的控制器方法
@Controller
@RequestMapping("/system/")
public class SysController {
    //通用跳转页面方法
    //跳转到111.html页面：localhost:8088/system/getPage.do?page=cms/111.html
    @RequestMapping("getPage.do")
    public String getPage(String page){
        return page;
    }

}
